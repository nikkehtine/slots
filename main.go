package main

import "fmt"

const MAX_LINES uint = 3

func deposit() uint {
	var amount uint
	for {
		fmt.Print("What would you like to deposit? $")
		_, err := fmt.Scanln(&amount)
		fmt.Println()
		if err != nil {
			fmt.Println("Type a valid amount.")
		} else {
			break
		}
	}
	return amount
}

func getNumberOfLines() uint {
	var lines uint
	for {
		fmt.Printf("How many lines do you want to bet on? (1-%d)? ", MAX_LINES)
		_, err := fmt.Scanln(&lines)
		fmt.Println()
		if err != nil || lines == 0 || lines > MAX_LINES {
			fmt.Println("Type a valid amount.")
		} else {
			break
		}
	}
	return lines
}

func main() {
	// balance := deposit()
}
